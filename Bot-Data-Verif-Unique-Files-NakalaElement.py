import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import json
import logging
import pathlib

def main(args=None):
    
    BASE_URL = "https://nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Unique Files dans un Element Nakala")

    listFichiers = []
    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        listFichiers = []
        for fichier in element["files"]:
            file_extension = pathlib.Path(fichier["name"]).suffix
            if file_extension != ".txt":
                listFichiers.append(fichier["name"])
        if len(listFichiers) > 1:
            logging.error("Bot Nakala Verif Data Unique Files : Trop de fichiers dans Element %s/%s" % ( BASE_URL, element["identifier"]))

if __name__ == "__main__":
    main()
