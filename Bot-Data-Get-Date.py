import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import logging
import csv
import json

def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verification date")
    f = open('Data/ListeDate.csv', 'w')
    writer = csv.writer(f)
    header = ["identifiant","date"]
    writer.writerow(header)
    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            date = Element.getMeta("date")
        except:
            pass
        finally:
            writer.writerow([Element.getMeta("identifier"), date])
    f.close()


if __name__ == "__main__":
    main()
