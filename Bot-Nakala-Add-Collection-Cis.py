import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import constants
import argparse


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Ajout Collection CIS")

    # ListeCBMA = ["11280/1e120e48","11280/e017dfb2","11280/5ee98294","11280/58c6f808","11280/253e7bc3","11280/e1eb464d","11280/ce0baace","11280/d8fb652a","11280/23179c51","11280/793869bf","11280/c1e47d04","11280/21579253","11280/0454fb76","11280/e5a01436","11280/18f3270f","11280/137f747e","11280/79951eaa","11280/0a2124ed","11280/ab0b16fa","11280/72a21873","11280/345435c7","11280/bc17d9a8","11280/be592904","11280/f6829812","11280/dc89e604","11280/87d52218","11280/2a248143"]
    ListeCBMA = ["10.34847/nkl.dc18go35"]
    NewCollection = ["10.34847/nkl.11d8ji55","10.34847/nkl.c362g2e9"]
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    for identifier in ListeCBMA:
        r = nklC.get_collections_datas(
            nklT_prodEmpty, identifier, page=1, limit=20
        )
        if r.isSuccess:
            lastPage = r.dictVals["lastPage"]
            for i in range(1, lastPage + 1):
                r_current = nklC.get_collections_datas(
                    nklT_prodEmpty, identifier, page=i, limit=20
                )
                if r_current.isSuccess:
                    logging.info(
                        "Chargement de la Page %s de la Collection %s/collection/%s"
                        % (i, nklT_prodEmpty.BASE_URL, identifier)
                    )
                    for element in r_current.dictVals["data"]:
                        nklD = nklAPI_Datas()
                        r_temp = nklD.get_datas_collections(
                            nklT_prodEmpty, element["identifier"]
                        )
                        if r_temp.isSuccess:
                            logging.info(
                                "Chargement des donnees de la Collections %s/%s"
                                % (nklT_prodEmpty.BASE_URL, element["identifier"])
                            )
                        else:
                            logging.error(
                                "Erreur de Chargement des données de la Collections %s/%s"
                                % (nklT_prodEmpty.BASE_URL, element["identifier"])
                            )
                        listetemp = []
                        for elementtemp in r_temp.dictVals:
                            listetemp.append(elementtemp["uri"][29:])
                        if NewCollection not in listetemp:
                            nklD = nklAPI_Datas()
                            r_newCollection = nklD.post_datas_collections(
                                nklT_prodEmpty, element["identifier"], NewCollection
                            )
                            if r_newCollection.isSuccess:
                                logging.info(
                                    "Modification de la Data %s/%s"
                                    % (nklT_prodEmpty.BASE_URL, element["identifier"])
                                )
                            else:
                                logging.error(
                                    "Erreur de Modification sur la Data %s/%s"
                                    % (nklT_prodEmpty.BASE_URL, element["identifier"])
                                )
                #            time.sleep(5)
                else:
                    logging.error(
                        "Error de Chargement de la Page %s de la Collection %s/collection/%s"
                        % (i, nklT_prodEmpty.BASE_URL, identifier)
                    )
        else:
            logging.error(
                "Error de Chargement de la Collection %s/collection/%s"
                % (nklT_prodEmpty.BASE_URL, identifier)
            )


if __name__ == "__main__":
    main()
