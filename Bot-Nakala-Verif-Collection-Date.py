import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NakalaCollection import NakalaCollection
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import argparse
import constants

def retourneAllNakalaDate(collection, lang="fr"):
    """retourne un set de tous les creators Nakala present dans les elements de la Collection"""
    listeCollectionNakalaDate = []
    nklT_prod = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prod, collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prod, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot date 2 Collection : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prod.BASE_URL, collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement(lang)
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    try:
                        NakalaDate = Element.getMeta("date")
                    except:
                        logging.info(
                            "Bot date 2 Collection : Pas de date dans la %s/collection/%s"
                            % (nklT_prod.BASE_URL, collection)
                        )
                    else:
                        listeCollectionNakalaDate.append(NakalaDate)
            else:
                logging.error(
                    "Bot date 2 Collection : Error de Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prod.BASE_URL, collection)
                )
    else:
        logging.error(
            "Bot date 2 Collection : Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prod.BASE_URL, collection)
        )
    return list(set(listeCollectionNakalaDate))


def retourneListCollectionDate(collection, lang="fr"):
    """retourne tous les creators Nakala affiche dans les metadonnes de l'element Collection"""
    Element = NakalaCollection(collection, lang=lang)
    date = []
    try:
        date = Element.getMeta("date")
    except:
        date = []
    return date

def ajoutCollectionDate(collection, Listedate):
    """ajout des creators dans la collection"""
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    lang = "fr"
    for element in Listedate:
        NewElement = NakalaElement(lang)
        NewElement.setMeta("date", element)
        dictValsNewMetas = NewElement.getMetaNakala()[0]
        r_metadata = nklC.post_collections_metadatas(nklT_prod, collection, dictValsNewMetas)
        if r_metadata.isSuccess:
            logging.info(
                "Bot date 2 Collection : Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, dictValsNewMetas)
            )
        else:
            logging.error(
                "Bot date 2 Collection : Erreur de Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, dictValsNewMetas)
            )

def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    lang = "fr"
    args = parser.parse_args()

    logging.info("Démarrage du Bot date 2 Collection")
    listeElementdate = []
    listeElementNakalaDate = retourneAllNakalaDate(
        collection=args.collection, lang=lang
    )
    listeCollectionDate = retourneListCollectionDate(
        collection=args.collection, lang=lang
    )

    diffdate = [
        x for x in listeElementNakalaDate if x not in listeCollectionDate
    ]
    if len(diffdate) != 0:
        ajoutCollectionDate(args.collection, diffdate)


if __name__ == "__main__":
    main()
