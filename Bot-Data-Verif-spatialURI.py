import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import json
import logging
import constants
import argparse
import re


def main(args=None):
    """règle REGEX"""
    re1 = "^http:\/\/www\.idref\.fr\/\d{8,9}(X|[0-9])\/id$"
    re2 = "^https?:\/\/catalogue\.bnf\.fr\/ark:\/12148\/cb(\d{8,9}[0-9bcdfghjkmnpqrstvwxz])"
    re3 = "^http:\/\/www\.wikidata\.org\/entity\/Q\d*$"
    re4 = "^https?:\/\/(?:www\.|geotree\.)?geonames\.org\/([1-9][0-9]{0,8})$"
    re5 = "^https?:\/\/ark\.frantiq\.fr\/ark:\/26678\/([0-9A-Za-z]{0,1}crt[0-9A-Za-z]{10})\/$"
    re6 = "^http:\/\/data\.culture\.fr\/thesaurus\/page\/ark:\/67717\/(T84-[1-9]\d{1,5}|[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12})$"
    compil = re.compile("(%s|%s|%s|%s|%s|%s)" % (re1, re2, re3, re4, re5, re6))

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verif Spatial URI")

    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        spatialsURIPresent = []
        for elementURI in ["spatialURI","provenanceURI","provenanceURI"]:
            try:
                spatialsURIPresent = Element.getMeta(elementURI)
            except:
                logging.error(
                    "Bot Nakala Verif Spatial URI : Erreur sur le chargement de Spatial sur la Page %s/%s"
                    % (elementURI, BASE_URL, dataIdentifier)
                    )
            finally:
                for element in spatialsURIPresent:
                    z = re.match(compil, element)
                    if not z:
                        logging.error(
<<<<<<< HEAD
                            "Bot Nakala Verif Spatial URI : Erreur sur lURI de la Page %s/%s avec %s pour %s"
                            % (BASE_URL, dataIdentifier, element, elementURI)
=======
                            "Bot Nakala Verif Spatial URI : Erreur sur lURI de la Page %s;%s/%s;%s"
                            % (elementURI, BASE_URL, dataIdentifier, element)
>>>>>>> d0c925086a413d7cec43c95e8c29ca8cf322c8df
                        )

if __name__ == "__main__":
    main()
