import sys

sys.path.append("nakala-pycon")

from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
import json
import logging
import argparse


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    args = parser.parse_args()
    logging.info("Démarrage du Bot Stats LaMOP")
    nbreElement = 0
    nbreMetadonnees = 0
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, args.collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, args.collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala Publisher : Chargement de la Page %s de la collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, args.collection)
                )
                nbreElement = len(r_current.dictVals["data"]) + nbreElement
                for element in r_current.dictVals["data"]:
                    nbreMetadonnees = len(element["metas"]) + nbreMetadonnees
            else:
                logging.info(
                    "Bot Nakala Publisher : Echec du chargement de la Page %s de la collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, args.collection)
                )
    else:
        logging.error(
            "Bot Nakala Publisher : Error de Chargement de la Page %s de la collection %s/collection/%s"
            % (i, nklT_prodEmpty.BASE_URL, args.collection)
        )
    print("Le nombre d'éléments est de : " + str(nbreElement))
    print("Le nombre de métadonnées est de : " + str(nbreMetadonnees))


if __name__ == "__main__":
    main()
