import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import constants


def ajoutMetadonnnee(nklT_prodEmpty, dataIdentifier, publisher, value, lang):
    NewElement = NakalaElement(lang)
    NewElement.setMeta(publisher, value)
    dictValsNewMetas = NewElement.getMetaNakala()
    nklD = nklAPI_Datas()
    r_metadata = nklD.post_datas_metadatas(
        nklT_prodEmpty, dataIdentifier, dictValsNewMetas
    )
    if r_metadata.isSuccess:
        logging.info(
            "Bot Nakala Publisher : Modification de la Data %s/%s avec %s"
            % (nklT_prodEmpty.BASE_URL, dataIdentifier, value)
        )
    else:
        logging.error(
            "Bot Nakala Publisher : Erreur de Modification de la Data %s/%s avec %s"
            % (nklT_prodEmpty.BASE_URL, dataIdentifier, value)
        )


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    collection = "11280/3335a4b7"
    logging.info("Démarrage du Bot Ajout PublisherURI CBMA")

    listeLaMOPURI = [
        "https://catalogue.bnf.fr/ark:/12148/cb11875434r",
        "http://www.wikidata.org/entity/Q19606443",
        "http://www.idref.fr/026521784/id",
    ]
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala Publisher : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("fr")
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    try:
                        publishersURIPresent = Element.getMeta("publishersURI")
                    except:
                        logging.error(
                            "Bot Nakala Publisher : Erreur sur le chargement de Publisher %s/collection/%s"
                            % (nklT_prodEmpty.BASE_URL, collection)
                        )
                        publishersURIPresent = []
                    else:
                        listeFinale = list(
                            set(listeLaMOPURI) - set(publishersURIPresent)
                        )
                        for element in listeFinale:
                            ajoutMetadonnnee(
                                nklT_prodEmpty,
                                dataIdentifier,
                                "publisherURI",
                                element,
                                "fr",
                            )
            else:
                logging.info(
                    "Bot Nakala Publisher : Echec du chargement de la collection %s/collection/%s"
                    % (nklT_prodEmpty.BASE_URL, dataIdentifier)
                )
    else:
        logging.error(
            "Bot Nakala Publisher : Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, collection)
        )


if __name__ == "__main__":
    main()
