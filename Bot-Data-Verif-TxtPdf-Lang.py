import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import constants
import logging
import json

def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Verification Language pour les Fichiers")

    BASE_URL = "https://nakala.fr"
    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        for fichier in element["files"]:
            if fichier["name"].endswith(".txt") or fichier["name"].endswith(".pdf"):
                Element = NakalaElement("")
                Element.fromNakalaDict(element)
                try:
                    language = Element.getMeta("language")
                    if len(language) == 0:
                        logging.error("Probleme : pas de language specifie pour le fichier %s/%s" % (BASE_URL, Element.getMeta("identifier")))
                except:
                    logging.error("Probleme Except: pas de language specifie pour le fichier %s/%s" % (BASE_URL, Element.getMeta("identifier")))

if __name__ == "__main__":
    main()
