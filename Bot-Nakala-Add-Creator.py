import sys
sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import constants
import argparse


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    args = parser.parse_args()

    logging.info("Démarrage du Bot Ajout Creator")

    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, args.collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals['lastPage']
        for i in range(1, lastPage+1):
            r_current = nklC.get_collections_datas(nklT_prodEmpty, args.collection, page=i, limit=20)
            if r_current.isSuccess:
                logging.info('Chargement de la Page %s de la Collection %s/collection/%s' % (i, nklT_prodEmpty.BASE_URL, args.collection))
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("fr")
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    AuteurNakalas = Element.getMeta("NakalaCreators")
                    if len(AuteurNakalas) == 0:
                        try:
                            Auteurs = Element.getMeta("creators")
                        except:
                            logging.error('Pas de Creators %s/%s avec %s' % (nklT_prodEmpty.BASE_URL, dataIdentifier))
                        else:
                            if len(Auteurs) != 0:
                    NewElement.getDictMetaAuthorDirect("CG21/F.PETOT/2021",)
                    NewElement.getDictMetaAuthorDirect(Auteurs[0],)
                    dictValsNewMetas = NewElement.getMetaNakala()[0]
                    nklD = nklAPI_Datas()
                    r_metadata = nklD.post_datas_metadatas(nklT_prodEmpty, dataIdentifier, dictValsNewMetas)
                    if r_metadata.isSuccess:
                        logging.info('Modification de la Data %s/%s' % (nklT_prodEmpty.BASE_URL, dataIdentifier))
                    else:
                        logging.error('Erreur de Modification de la Data %s/%s' % (nklT_prodEmpty.BASE_URL, dataIdentifier))
            else:
                logging.error('Error de Chargement de la Page %s de la Collection %s/collection/%s' % (i, nklT_prodEmpty.BASE_URL, args.collection))
    else:
        logging.error('Error de Chargement de la Collection %s/collection/%s' % (nklT_prodEmpty.BASE_URL, args.collection))

if __name__ == "__main__":
    main()
