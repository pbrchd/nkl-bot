import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import json
import logging
import constants
import argparse
import re


def main(args=None):
    """règle REGEX"""
    re1 = "^http:\/\/www\.idref\.fr\/\d{8,10}\/id$"
    re2 = "^https?:\/\/aurehal\.archives-ouvertes\.fr\/structure\/([1-9]\d{0,6})"
    re3 = "^https?:\/\/catalogue\.bnf\.fr\/ark:\/12148\/cb(\d{8,9}[0-9bcdfghjkmnpqrstvwxz])"
    re4 = "^https?:\/\/scanr\.enseignementsup-recherche\.gouv\.fr\/(structure|entite)\/\d*[A-Z]$"
    re5 = "^http:\/\/www\.wikidata\.org\/entity\/Q\d*$"
    re6 = "^https?:\/\/ror\.org\/([a-z0-9]{9})$"
    compil = re.compile("(%s|%s|%s|%s|%s|%s)" % (re1, re2, re3, re4, re5, re6))

    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    args = parser.parse_args()

    logging.info("Démarrage du Bot Nakala Verif Publisher URI")

    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, args.collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, args.collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala Verif Publisher URI : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, args.collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("fr")
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    try:
                        publishersURIPresent = Element.getMeta("publisherURI")
                    except:
                        logging.error(
                            "Bot Nakala Verif Publisher URI : Erreur sur le chargement de Publisher sur la Page %s/%s"
                            % (nklT_prodEmpty.BASE_URL, dataIdentifier)
                        )
                        publishersURIPresent = []
                    finally:
                        for element in publishersURIPresent:
                            z = re.match(compil, element)
                            if not z:
                                logging.error(
                                    "Bot Nakala Verif Publisher URI : Erreur sur lURI de la Page %s/%s avec %s"
                                    % (nklT_prodEmpty.BASE_URL, dataIdentifier, element)
                                )
            else:
                logging.info(
                    "Bot Nakala Verif Publisher URI: Echec du chargement de la collection %s/collection/%s"
                    % (nklT_prodEmpty.BASE_URL, dataIdentifier)
                )
    else:
        logging.error(
            "Bot Nakala Verif Publisher URI: Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, args.collection)
        )


if __name__ == "__main__":
    main()
