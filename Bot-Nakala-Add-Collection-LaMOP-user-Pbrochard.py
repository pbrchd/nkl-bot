# -*- coding: utf-8 -*-
"""

Created : @author: pbrochard

"""

import sys

sys.path.append("nakala-pycon")

from NklTarget import NklTarget
from nklAPI_Users import nklAPI_Users
from nklAPI_Datas import nklAPI_Datas
import time
import logging
import constants
import argparse


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Ajout Collection LaMOP")
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklU = nklAPI_Users()
    page = 20
    NewCollection = "11280/43622a63"
    r = nklU.post_users_datas(nklT_prod, "deposited", 1, 10)
    if r.isSuccess:
        nbrePages = int(int(r.dictVals["totalRecords"]) / page + 1)
        logging.info("Nombre de pages à traiter %s" % (nbrePages))
    else:
        logging.error("Erreur sur le nombre de pages")

    for i in range(1, nbrePages):
        r = nklU.post_users_datas(nklT_prod, "deposited", i, page)
        if r.isSuccess:
            for element in r.dictVals["data"]:
                if NewCollection not in element["collectionsIds"]:
                    nklD = nklAPI_Datas()
                    r_newCollection = nklD.post_datas_collections(
                        nklT_prod, element["identifier"], [NewCollection]
                    )
                    if r_newCollection.isSuccess:
                        logging.info(
                            "Modification de la Data %s/%s"
                            % (nklT_prod.BASE_URL, element["identifier"])
                        )
                    else:
                        logging.error(
                            "Modification de la Data %s/%s"
                            % (nklT_prod.BASE_URL, element["identifier"])
                        )
        else:
            logging.error("Erreur sur la page %s" % (i))


if __name__ == "__main__":
    main()
