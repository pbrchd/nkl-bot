import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import constants


def ajoutMetadonnnee(nklT_prod, dataIdentifier, relation, value, lang):
    NewElement = NakalaElement(lang=lang)
    NewElement.setMeta(relation, value)
    dictValsNewMetas = NewElement.getMetaNakala()[0]
    nklD = nklAPI_Datas()
    r_metadata = nklD.post_datas_metadatas(nklT_prod, dataIdentifier, dictValsNewMetas)
    if r_metadata.isSuccess:
        logging.info(
            "Bot Nakala relation : Modification de la Data %s/%s avec %s"
            % (nklT_prod.BASE_URL, dataIdentifier, value)
        )
    else:
        logging.error(
            "Bot Nakala relation : Erreur de Modification de la Data %s/%s avec %s"
            % (nklT_prod.BASE_URL, dataIdentifier, value)
        )


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    collection = "11280/3335a4b7"
    logging.info("Démarrage du Bot Ajout relationURI CBMA")

    listeCBMAURI = ["http://www.cbma-project.eu/"]
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prod, collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prod, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala relation : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prod.BASE_URL, collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("fr")
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    try:
                        relationURIPresent = Element.getMeta("relationURI")
                    except:
                        logging.error(
                            "Bot Nakala relation : Erreur sur le chargement de relation %s/collection/%s"
                            % (nklT_prod.BASE_URL, collection)
                        )
                        relationURIPresent = []
                    else:
                        listeFinale = list(set(listeCBMAURI) - set(relationURIPresent))
                        for element in listeFinale:
                            ajoutMetadonnnee(
                                nklT_prod, dataIdentifier, "relationURI", element, "fr"
                            )
            else:
                logging.info(
                    "Bot Nakala relation : Echec du chargement de la collection %s/collection/%s"
                    % (nklT_prod.BASE_URL, dataIdentifier)
                )
    else:
        logging.error(
            "Bot Nakala relation : Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prod.BASE_URL, collection)
        )


if __name__ == "__main__":
    main()
