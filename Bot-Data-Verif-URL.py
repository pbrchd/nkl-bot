import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import requests

def testURL(listURL):
    '''test des urls'''
    for element in listURL:
        logging.info(
            "Chargement de URL %s" % (element)
        )
        if element.startswith("https://www.nakala.fr"):
            nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
            nklD = nklAPI_Datas()
            rd = nklD.get_datas(nklT_prodEmpty, element)
            if not rd.isSuccess:
                logging.error(
                    "Error sur le Chargement de URL %s"
                    % (element)
                )
        else:
            r = requests.get(element)
            if r.status_code == 400 or r.status_code == 404:
                logging.error(
                    "Error sur le Chargement de URL %s"
                    % (element)
                )
        time.sleep(15)


def main(args=None):

    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verif Publisher URI")

    f = open('data.json')
    listURL = []
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        element = Element.getMeta("identifier")
        for element in ["publisherURI","relationURI","spatialURI", "provenanceURI","sourceURI"]:
            try:
                listURL.extend(Element.getMeta(element))
            except:
                pass
    testURL(list(set(listURL)))

if __name__ == "__main__":
    main()
