import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import csv
import json
import logging

def main(args=None):

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Get NakalaCreator")

    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            temp = Element.getMeta("nakalacreator")
        except:
            logging.error("Probleme : Erreur de Nakala Creator %s/%s" % (BASE_URL, Element.getMeta("identifier")))
        else:
            if len(temp) == 0:
                logging.error("Probleme : Pas de Nakala Creator %s/%s" % (BASE_URL, Element.getMeta("identifier")))


if __name__ == "__main__":
    main()
