import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import json
import logging
import argparse


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Stats LaMOP")
    nbreElement = 0
    nbreMetadonnees = 0
    listNakalaCreator = []
    f = open('data.json')
    data = json.load(f)
    nbreElement = len(data["data"])
    for element in data["data"]:
        nbreMetadonnees = len(element["metas"]) + nbreMetadonnees
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        try:
            listNakalaCreator.extend(Element.getMeta("nakalacreator"))
        except:
            pass
    print("Le nombre d'éléments est de : " + str(nbreElement))
    print("Le nombre de métadonnées est de : " + str(nbreMetadonnees))
    print("Le nombre de creator est de : " + str(len(list(set(listNakalaCreator)))))


if __name__ == "__main__":
    main()
