import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import json
import logging
import constants
from dateutil.parser import *

def main(args=None):

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verif Description")

    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            date = Element.getMeta("date")
        except:
            logging.error(
                "Bot Nakala Verif Date : Erreur Pas de date sur la Page %s/%s"
                % (BASE_URL, dataIdentifier)
                )
        finally:
            if "/" in date:
                listeAnnee = date.split("/")
                for Annee in listeAnnee:
                    try:
                        temp = isoparse(Annee)
                    except:
                        logging.error(
                            "Bot Nakala Verif Date : Element sans Date Valable sur la Page %s/%s"
                            % (
                            BASE_URL, dataIdentifier,
                            )
                        )
            else:
                try:
                    isoparse(date)
                except:
                    logging.error(
                        "Bot Nakala Verif Date : Element sans Date Valable sur la Page %s/%s"
                        % (
                            BASE_URL, dataIdentifier,
                        )
                    )

if __name__ == "__main__":
    main()
