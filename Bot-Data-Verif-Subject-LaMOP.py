import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import csv
import json
import logging

def main(args=None):

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verif Subject")

    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            temp = Element.getMeta("subject")
        except:
            logging.error("Probleme : pas de Mots Clefs %s/%s" % (BASE_URL, Element.getMeta("identifier")))
        else:
            if "LaMOP" not in temp:
                logging.error("Probleme : pas de Mots Clefs LaMOP dans %s/%s" % (BASE_URL, Element.getMeta("identifier")))
            if "Laboratoire de Médiévistique Occidentale de Paris" in temp or "Laboratoire de Médiévistique occidentale de Paris" in temp or "Laboratoire de médiévistique occidentale de Paris" in temp:
                pass
            else:
                logging.error("Probleme : pas de Mots Clefs Laboratoire Médievistique dans %s/%s" % (BASE_URL, Element.getMeta("identifier")))


if __name__ == "__main__":
    main()
