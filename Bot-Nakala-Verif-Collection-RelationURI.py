import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NakalaCollection import NakalaCollection
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import argparse
import constants


def retourneAllNakalaRelationURI(collection, lang="fr"):
    """retourne un set de tous les creators Nakala present dans les elements de la Collection"""
    listeCollectionNakalaRelationURI = []
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot RelationURI 2 Collection : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement(lang)
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    NakalaRelationURI = Element.getMeta("relationURI")
                    listeCollectionNakalaRelationURI.extend(NakalaRelationURI)
            else:
                logging.error(
                    "Bot RelationURI 2 Collection : Error de Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
    else:
        logging.error(
            "Bot RelationURI 2 Collection : Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, collection)
        )
    return set(listeCollectionNakalaRelationURI)


def retourneListCollectionRelationURI(collection, lang="fr"):
    """retourne tous les creators Nakala affiche dans les metadonnes de l'element Collection"""
    Element = NakalaCollection(collection, lang=lang)
    return Element.creator


def ajoutCollectionRelationURI(collection, ListeRelationURI):
    """ajout des creators dans la collection"""
    Element = NakalaCollection()
    Element.setMeta("relationURI", ListeRelationURI)
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    for element in Element.getMetaNakala():
        r_metadata = nklC.post_collections_metadatas(nklT_prod, collection, element)
        if r_metadata.isSuccess:
            logging.info(
                "Bot RelationURI 2 Collection : Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, element)
            )
        else:
            logging.error(
                "Bot RelationURI 2 Collection : Erreur de Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, element)
            )


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    lang = "fr"
    args = parser.parse_args()

    logging.info("Démarrage du Bot RelationURI 2 Collection")
    listeElementRelationURI = []
    listeElementNakalaRelationURI = retourneAllNakalaRelationURI(
        collection=args.collection, lang=lang
    )
    listeCollectionRelationURI = retourneListCollectionRelationURI(
        collection=args.collection, lang=lang
    )

    diffRelationURI = [
        x for x in listeElementNakalaRelationURI if x not in listeCollectionRelationURI
    ]
    if len(diffRelationURI) != 0:
        ajoutCollectionRelationURI(args.collection, diffRelationURI)
        logging.info(
            "Bot RelationURI 2 Collection : Il manquait des RelationURI sur la Collection %s/collection/%s"
                % (nklT_prodEmpty.BASE_URL, args.collection)
        )
    else:
        logging.info(
            "Bot RelationURI 2 Collection : RelationURI OK sur la Collection %s/collection/%s"
                % (nklT_prodEmpty.BASE_URL, args.collection)
        )


if __name__ == "__main__":
    main()
