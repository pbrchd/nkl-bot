import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import csv
import json
import logging
from difflib import SequenceMatcher

def similair(listSimilair):
    '''trouvez les ressemblances'''
    ratio = 0.8
    for value1 in listSimilair:
        for value2 in listSimilair:
            if SequenceMatcher(None,value1 ,value2).ratio() >= ratio and SequenceMatcher(None,value1 ,value2).ratio() != 1:
                logging.error("Probleme : Similair  %s et %s" % (value1, value2))

def main(args=None):
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )

    logging.info("Démarrage du Bot Data Get Similair")
    listSimilair = []
    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        for element in ["publisher","spatial","provenance"]:
            try:
                temp = Element.getMeta(element)
            except:
                pass
            finally:
                listSimilair.extend(Element.getMeta(element))
    similair(list(set(listSimilair)))
    f.close()


if __name__ == "__main__":
    main()
