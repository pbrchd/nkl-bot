import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import csv
import json
import logging

def main(args=None):
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )

    logging.info("Démarrage du Bot Nakala Get source")
    BASE_URL = "https://www.nakala.fr"
    f = open('Data/ListeSource.csv', 'w')
    writer = csv.writer(f)
    header = ["identifiant","Source"]
    writer.writerow(header)
    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            temp = Element.getMeta("source")
        except:
            logging.error("Probleme : pas de source %s/%s" % (BASE_URL, Element.getMeta("identifier")))
        finally:
            for element in Element.getMeta("source"):
                    writer.writerow([Element.getMeta("identifier"), element])
    f.close()


if __name__ == "__main__":
    main()
