import sys

sys.path.append("nakala-pycon")

from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from NakalaElement import NakalaElement
from nklAPI_Users import nklAPI_Users
import constants
import logging


def test_elements_collection(typeFichier, collection):
    '''test les elements dans les collections'''
    nklC = nklAPI_Collections()
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    r = nklC.get_collections_datas(nklT_prodEmpty, collection)
    listData = []
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r = nklC.get_collections_datas(nklT_prodEmpty, collection, page=i)
            for element in r.dictVals["data"]:
                Element = NakalaElement("fr")
                Element.fromNakalaDict(element)
                fichierText = False
                for fichier in element["files"]:
                    if fichier["name"].endswith(typeFichier):
                        fichierText = True
                if fichierText == False:
                    logging.error("Error : pas de fichier %s sur la Page %s/%s" % (typeFichier, nklT_prodEmpty.BASE_URL, Element.identifier))


def retourne_users_collections():
    ''' retourne la liste des collections'''
    nklT = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklU = nklAPI_Users()
    page = 50
    r = nklU.post_users_collections(nklT, "deposited", 1, 10)
    if r.isSuccess:
        nbrePages = int(int(r.dictVals["totalRecords"])/page + 1)
    else:
        logging.error(
                "Bot Nakala Verif Type Fichier : Error de Chargement des Collections"
        )

    for i in range(1, nbrePages+1):
        r = nklU.post_users_collections(nklT, "deposited", i, page)
        if r.isSuccess:
            for element in r.dictVals["data"]:
                for metas in element["metas"]:
                    if metas["propertyUri"] == "http://nakala.fr/terms#title" and metas["lang"] == "fr" and metas["typeUri"] == "http://www.w3.org/2001/XMLSchema#string" and "Textes" in metas["value"]:
                        test_elements_collection(".txt", element["identifier"])
                    if metas["propertyUri"] == "http://nakala.fr/terms#title" and metas["lang"] == "fr" and metas["typeUri"] == "http://www.w3.org/2001/XMLSchema#string" and "PageXML" in metas["value"]:
                        test_elements_collection(".xml", element["identifier"])
                    if metas["propertyUri"] == "http://nakala.fr/terms#title" and metas["lang"] == "fr" and metas["typeUri"] == "http://www.w3.org/2001/XMLSchema#string" and "PDF" in metas["value"]:
                        test_elements_collection(".pdf", element["identifier"])
        else:
            logging.error(
                    "Bot Nakala Verif Type Fichier : Error de Chargement des Collections"
            )
                
def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Verification du Type de Fichiers")

    retourne_users_collections()


if __name__ == "__main__":
    main()
