import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NakalaCollection import NakalaCollection
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import argparse
import constants


def retourneAllNakalaSubject(collection, lang="fr"):
    """retourne un set de tous les creators Nakala present dans les elements de la Collection"""
    listeCollectionNakalaSubject = []
    nklT_prod = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prod, collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Subject 2 Collection : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement(lang)
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    NakalaSubject = Element.getMeta("subject")
                    listeCollectionNakalaSubject.extend(NakalaSubject)
            else:
                logging.error(
                    "Bot Subject 2 Collection : Error de Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, identifier)
                )
    else:
        logging.error(
            "Bot Subject 2 Collection : Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, collection)
        )
    return set(listeCollectionNakalaSubject)


def retourneListCollectionSubject(collection, lang="fr"):
    """retourne tous les creators Nakala affiche dans les metadonnes de l'element Collection"""
    Element = NakalaCollection(collection, lang=lang)
    return Element.creator


def ajoutCollectionSubject(collection, ListeSubject):
    """ajout des creators dans la collection"""
    Element = NakalaCollection()
    Element.setMeta("subject", ListeSubject)
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    for element in Element.getMetaNakala():
        r_metadata = nklC.post_collections_metadatas(nklT_prod, collection, element)
        if r_metadata.isSuccess:
            logging.info(
                "Bot Subject 2 Collection : Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, element)
            )
        else:
            logging.error(
                "Bot Subject 2 Collection : Erreur de Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, element)
            )


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    lang = "fr"
    args = parser.parse_args()

    logging.info("Démarrage du Bot Subject 2 Collection")
    listeElementSubject = []
    listeElementNakalaSubject = retourneAllNakalaSubject(
        collection=args.collection, lang=lang
    )
    listeCollectionSubject = retourneListCollectionSubject(
        collection=args.collection, lang=lang
    )

    diffSubject = [
        x for x in listeElementNakalaSubject if x not in listeCollectionSubject
    ]
    if len(diffSubject) != 0:
        ajoutCollectionSubject(args.collection, diffSubject)
        logging.info(
            "Bot Subject 2 Collection : Il manquait des Subject sur la Collection /collection/%s"
                % (args.collection)
        )
    else:
        logging.info(
            "Bot Subject 2 Collection : Subject OK sur la Collection /collection/%s"
                % (args.collection)
        )


if __name__ == "__main__":
    main()
