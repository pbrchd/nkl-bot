import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NakalaCollection import NakalaCollection
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import argparse
import constants


def retourneAllNakalaProvenance(collection, lang="fr"):
    """retourne un set de tous les creators Nakala present dans les elements de la Collection"""
    listeCollectionNakalaProvenance = []
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Provenance 2 Collection : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement(lang)
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    NakalaProvenance = Element.getMeta("provenance")
                    listeCollectionNakalaProvenance.extend(NakalaProvenance)
            else:
                logging.error(
                    "Bot Provenance 2 Collection : Error de Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
    else:
        logging.error(
            "Bot Provenance 2 Collection : Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, collection)
        )
    return set(listeCollectionNakalaProvenance)


def retourneListCollectionProvenance(collection, lang="fr"):
    """retourne tous les creators Nakala affiche dans les metadonnes de l'element Collection"""
    Element = NakalaCollection(collection, lang=lang)
    return Element.creator


def ajoutCollectionProvenance(collection, ListeProvenance):
    """ajout des creators dans la collection"""
    Element = NakalaCollection()
    Element.setMeta("provenance", ListeProvenance)
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    for element in Element.getMetaNakala():
        r_metadata = nklC.post_collections_metadatas(nklT_prod, collection, element)
        if r_metadata.isSuccess:
            logging.info(
                "Bot Provenance 2 Collection : Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, element)
            )
        else:
            logging.error(
                "Bot Provenance 2 Collection : Erreur de Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, element)
            )


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    lang = "fr"
    args = parser.parse_args()

    logging.info("Démarrage du Bot Provenance 2 Collection")
    listeElementProvenance = []
    listeElementNakalaProvenance = retourneAllNakalaProvenance(
        collection=args.collection, lang=lang
    )
    listeCollectionProvenance = retourneListCollectionProvenance(
        collection=args.collection, lang=lang
    )

    diffProvenance = [
        x for x in listeElementNakalaProvenance if x not in listeCollectionProvenance
    ]
    if len(diffProvenance) != 0:
        ajoutCollectionProvenance(args.collection, diffProvenance)


if __name__ == "__main__":
    main()
