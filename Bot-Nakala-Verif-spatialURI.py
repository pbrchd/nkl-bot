import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import json
import logging
import constants
import argparse
import re


def main(args=None):
    """règle REGEX"""
    re1 = "^http:\/\/www\.idref\.fr\/\d{8,10}\/id$"
    re2 = "^https?:\/\/catalogue\.bnf\.fr\/ark:\/12148\/cb(\d{8,9}[0-9bcdfghjkmnpqrstvwxz])"
    re3 = "^http:\/\/www\.wikidata\.org\/entity\/Q\d*$"
    re4 = "^https?:\/\/(?:www\.|geotree\.)?geonames\.org\/([1-9][0-9]{0,8})$"
    re5 = "^https?:\/\/ark\.frantiq\.fr\/ark:\/26678\/([0-9A-Za-z]{0,1}crt[0-9A-Za-z]{10})\/$"
    re6 = "^http:\/\/data\.culture\.fr\/thesaurus\/page\/ark:\/67717\/(T84-[1-9]\d{1,5}|[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12})$"
    compil = re.compile("(%s|%s|%s|%s|%s|%s)" % (re1, re2, re3, re4, re5, re6))

    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    args = parser.parse_args()

    logging.info("Démarrage du Bot Nakala Verif Spatial URI")

    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, args.collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, args.collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala Verif Spatial URI : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, args.collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("fr")
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    for elementURI in ["spatialURI","provenanceURI"]:
                        try:
                            spatialURIPresent = Element.getMeta(elementURI)
                        except:
                            logging.error(
                                "Bot Nakala Verif Spatial URI : Erreur sur le chargement de %s sur la Page %s/%s"
                                % (elementURI, nklT_prodEmpty.BASE_URL, dataIdentifier)
                            )
                            spatialURIPresent = []
                        finally:
                            for element in spatialURIPresent:
                                z = re.match(compil, element)
                                if not z:
                                    logging.error(
                                        "Bot Nakala Verif Spatial URI : Erreur sur lURI de la Page %s/%s avec %s"
                                        % (nklT_prodEmpty.BASE_URL, dataIdentifier, element)
                                    )
            else:
                logging.info(
                    "Bot Nakala Verif Spatial URI: Echec du chargement de la collection %s/collection/%s"
                    % (nklT_prodEmpty.BASE_URL, dataIdentifier)
                )
    else:
        logging.error(
            "Bot Nakala Verif Spatial URI: Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, args.collection)
        )


if __name__ == "__main__":
    main()
