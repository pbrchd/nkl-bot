import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from nklAPI_Collections import nklAPI_Collections
from NklTarget import NklTarget
import time
import logging
import json

def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    collection = "10.34847/nkl.28632y7o"
    #collection = "11280/43622a63"
    listData = []
    dictData = {}
    logging.info("Démarrage du Bot Récupération Data")
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
                listData.extend(r_current.dictVals["data"])
            else:
                logging.error(
                    "Error de Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
            time.sleep(5)
    else:
        logging.error(
            "Error de Chargement de la Page %s de la Collection %s/collection/%s"
            % (i, nklT_prodEmpty.BASE_URL, collection)
        )
    dictData["data"] = listData
    with open('data.json', 'w', encoding='utf-8') as f:
        json.dump(dictData, f, ensure_ascii=False, indent=4)

if __name__ == "__main__":
    main()
