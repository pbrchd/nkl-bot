import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import csv
import json
import logging

def main(args=None):
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Get NakalaCreator")
    fichier = open('Data/ListeNakalaCreator.csv', 'w')
    writer = csv.writer(fichier)
    header = ["identifiant","NakalaCreator"]
    writer.writerow(header)
    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            temp = Element.getMeta("nakalacreator")
        except:
            logging.error("Probleme : pas de Nakala Creator %s/%s" % (BASE_URL, Element.getMeta("identifier")))
        finally:
            for element in Element.getMeta("nakalacreator"):
                    writer.writerow([Element.getMeta("identifier"), element])


if __name__ == "__main__":
    main()
