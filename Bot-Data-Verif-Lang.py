import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import json
import logging
import constants
import argparse
import re


def main(args=None):

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verif Lang")

    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
         for ele in element["metas"]:
            if (
                ele["propertyUri"] != "http://nakala.fr/terms#license"
                or ele["propertyUri"] != "http://nakala.fr/terms#created"
                or ele["propertyUri"] != "http://nakala.fr/terms#creator"
                ):
                pass
            else:
                try:
                    temp = ele["lang"]
                except:
                    logging.error(
                                "Bot Data Verif Lang : Element sans Lang avec %s"
                                % (ele)
                            )

if __name__ == "__main__":
    main()
