import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import json
import logging
import constants
import argparse
import re


def main(args=None):
    """règle REGEX"""
    re1 = "^http:\/\/www\.idref\.fr\/\d{8,9}(X|[0-9])\/id$"
    re2 = "^https?:\/\/aurehal\.archives-ouvertes\.fr\/structure\/([1-9]\d{0,6})"
    re3 = "^https?:\/\/catalogue\.bnf\.fr\/ark:\/12148\/cb(\d{8,9}[0-9bcdfghjkmnpqrstvwxz])"
    re4 = "^https?:\/\/scanr\.enseignementsup-recherche\.gouv\.fr\/(structure|entite)\/\d*[A-Z]$"
    re5 = "^http:\/\/www\.wikidata\.org\/entity\/Q\d*$"
    re6 = "^https?:\/\/ror\.org\/([a-z0-9]{9})$"
    re7 = "^https?:\/\/(?:www\.|geotree\.)?geonames\.org\/([1-9][0-9]{0,8})$"
    compil = re.compile("(%s|%s|%s|%s|%s|%s|%s)" % (re1, re2, re3, re4, re5, re6, re7))

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verif Creator URI")

    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        creatorsURIPresent = []
        try:
            creatorsURIPresent = Element.getMeta("creatorURI")
        except:
            logging.error(
                "Bot Nakala Verif Creator URI : Erreur sur le chargement de Creator sur la Page %s/%s"
                % (BASE_URL, dataIdentifier)
                )
        finally:
            for element in creatorsURIPresent:
                z = re.match(compil, element)
                if not z:
                    logging.error(
                        "Bot Nakala Verif Creator URI : Erreur sur lURI de la Page %s/%s avec %s pour creatorURI"
                        % (BASE_URL, dataIdentifier, element)
                    )

if __name__ == "__main__":
    main()
