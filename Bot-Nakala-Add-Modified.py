import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import constants


def renvoiemodDate(dataIdentifier):
    """renvoie modDate"""

    nklT_prod = NklTarget(isNakalaProd=True, apiKey="")
    nklD = nklAPI_Datas()
    r_current = nklD.get_datas(nklT_prod, dataIdentifier)
    if r_current.isSuccess:
        logging.info(
            "Outil Creation 2 Manifest : Chargement de la Page %s/%s"
            % (nklT_prod.BASE_URL, dataIdentifier)
        )
        if r_current.dictVals["modDate"] is None:
            modDate = r_current.dictVals["creDate"][0:10]
        else:
            modDate = r_current.dictVals["modDate"][0:10]
    else:
        logging.error(
            "Bot Nakala Ajout Modified : Error de Chargement de la Page %s/%s"
            % (nklT_prod.BASE_URL, dataIdentifier)
        )
    return modDate


def ajoutMetadonnnee(nklT_prod, dataIdentifier, modified, value, lang):
    NewElement = NakalaElement(lang=lang)
    NewElement.setMeta(modified, value)
    dictValsNewMetas = NewElement.getMetaNakala()[0]
    dictValsNewMetas["typeUri"] = "http://www.w3.org/2001/XMLSchema#date"
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklD = nklAPI_Datas()
    r_metadata = nklD.post_datas_metadatas(nklT_prod, dataIdentifier, dictValsNewMetas)
    if r_metadata.isSuccess:
        logging.info(
            "Bot Nakala Ajout Modified : Modification de la Data %s/%s avec %s"
            % (nklT_prod.BASE_URL, dataIdentifier, value)
        )
    else:
        logging.error(
            "Bot Nakala Ajout Modified : Erreur de Modification de la Data %s/%s avec %s"
            % (nklT_prod.BASE_URL, dataIdentifier, value)
        )


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    collection = "11280/43622a63"
    logging.info("Démarrage du Bot Ajout Modified")

    nklT_prod = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prod, collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prod, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala Ajout Modified : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prod.BASE_URL, collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("fr")
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    try:
                        modifiedPresent = Element.getMeta("modified")
                    except:
                        logging.error(
                            "Bot Nakala Ajout Modified : Erreur sur le chargement de %s/%s"
                            % (nklT_prod.BASE_URL, dataIdentifier)
                        )
                        modifiedPresent = []
                    else:
                        modDate = renvoiemodDate(dataIdentifier)
                        if modDate not in modifiedPresent:
                            ajoutMetadonnnee(
                                nklT_prod, dataIdentifier, "modified", modDate, "fr"
                            )
            else:
                logging.info(
                    "Bot Nakala Ajout Modified : Echec du chargement de la Page %s de la collection %s/collection/%s"
                    % (i, nklT_prod.BASE_URL, dataIdentifier)
                )
    else:
        logging.error(
            "Bot Nakala Ajout Modified : Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prod.BASE_URL, collection)
        )


if __name__ == "__main__":
    main()
