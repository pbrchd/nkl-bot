import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import constants
import argparse


def ajoutMetadonnnee(nklT_prodEmpty, dataIdentifier, publisher):
    NewElement = NakalaElement(lang="fr")
    NewElement.setMeta("publisher", publisher)
    dictValsNewMetas = NewElement.getMetaNakala()[0]
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)

    nklD = nklAPI_Datas()
    r_metadata = nklD.post_datas_metadatas(
        nklT_prod, dataIdentifier, dictValsNewMetas
    )
    if r_metadata.isSuccess:
        logging.info(
            "Bot Nakala publisher : Modification de la Data %s/%s avec %s"
            % (nklT_prod.BASE_URL, dataIdentifier, publisher)
        )
    else:
        logging.error(
            "Bot Nakala publisher : Erreur de Modification de la Data %s/%s avec %s"
            % (nklT_prod.BASE_URL, dataIdentifier, publisher)
        )


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Ajout Publisher CBMA, Corpus etc..")

    collection = "11280/3335a4b7"
    CBMA = "Projet CBMA - Corpus Burgundiae Medii Aevi"
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(
        nklT_prodEmpty, collection, page=1, limit=20
    )
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala publisher : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("fr")
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    listePublisherAjoutes = []
                    listePublisherPresents = Element.getMeta("publisher")
                    if CBMA not in listePublisherPresents:
                        ajoutMetadonnnee(
                            nklT_prodEmpty,
                            dataIdentifier,
                            CBMA
                        )
            else:
                logging.info(
                    "Bot Nakala publisher : Echec du chargement de la collection %s/%s"
                    % (nklT_prodEmpty.BASE_URL, dataIdentifier)
                )
    else:
        logging.error(
            "Bot Nakala publisher : Error de Chargement de la Page %s de la Collection %s/collection/%s"
            % (i, nklT_prodEmpty.BASE_URL, collection)
        )


if __name__ == "__main__":
    main()
