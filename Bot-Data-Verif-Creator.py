import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import csv
import json
import logging

def main(args=None):

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Get Creator")
    listeAuteur = []

    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            temp = Element.getMeta("creator")
        except:
            logging.error("Probleme : Erreur de Creator %s/%s" % (BASE_URL, Element.getMeta("identifier")))
        else:
            if temp == []:
                logging.error("Probleme : Pas de Creator %s/%s avec comme titre : %s" % (BASE_URL, Element.getMeta("identifier"), Element.getMeta("title")))

if __name__ == "__main__":
    main()
