import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import json
import logging
import constants


def main(args=None):

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verif Description")

    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            descriptionPresent = Element.getMeta("description")
        except:
            logging.error(
                "Bot Nakala Verif Description : Erreur Pas de description sur la Page %s/%s"
                % (BASE_URL, dataIdentifier)
                )

if __name__ == "__main__":
    main()
