import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import json
import logging


def similair(listSimilair):
    '''trouvez les ressemblances'''
    nbreElement = len(listSimilair)
    nbreFiltre = len(list(set(listSimilair)))
    if nbreElement == nbreFiltre:
        logging.error("Probleme : il y a des fichiers avec le même nom")

def main(args=None):

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Unique Files")

    listFichiers = []
    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        for fichier in element["files"]:
            listFichiers.append(fichier["name"])
    similair(listFichiers)

if __name__ == "__main__":
    main()
