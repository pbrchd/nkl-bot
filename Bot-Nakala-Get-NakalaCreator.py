import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from nklAPI_Collections import nklAPI_Collections
from NklTarget import NklTarget
import time
import logging
import argparse
import csv

def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    args = parser.parse_args()

    logging.info("Démarrage du Bot Verification NakalaCreator")
    f = open('ListeNakalaCreator.csv', 'w')
    writer = csv.writer(f)
    header = ["identifiant","publisher"]
    writer.writerow(header)
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, args.collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, args.collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, args.collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("fr")
                    try:
                        Element.fromNakalaDict(element)
                    except:
                        logging.error(
                            "Probleme avec les metadonnes %s/%s"
                            % (nklT_prodEmpty.BASE_URL, Element.getMeta("identifier"))
                        )
                    else:
                        try:
                            for element in Element.getMeta("nakalacreator"):
                                writer.writerow([Element.getMeta("identifier"), element])
                        except:
                            logging.error(
                                "Pas de NakalaCreator %s/%s"
                                % (
                                    nklT_prodEmpty.BASE_URL,
                                    Element.getMeta("identifier"),
                                )
                            )
            else:
                logging.error(
                    "Error de Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, args.collection)
                )

    f.close()


if __name__ == "__main__":
    main()
