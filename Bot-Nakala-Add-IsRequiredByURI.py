import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import NklTarget as nklT
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
import posixpath
import json
import logging
import constants
import argparse

def ajoutMetadonnnee(manuscritIdentifier, isRequiredByURI):
    '''ajout SourceURI sur les textes'''
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    NewElement = NakalaElement("fr")
    sourceURI = posixpath.join(nklT_prod.BASE_URL, manuscritIdentifier[0], manuscritIdentifier[1])
    NewElement.setMeta("sourceURI", sourceURI)
    texteIdentifier = isRequiredByURI.split("/")[-2] + "/" + isRequiredByURI.split("/")[-1]
    dictValsNewMetas = NewElement.getMetaNakala()[0]
    nklD = nklAPI_Datas()
    r_metadata = nklD.post_datas_metadatas(
        nklT_prod, texteIdentifier, dictValsNewMetas
    )
    if r_metadata.isSuccess:
        logging.info(
            "Bot Nakala isRequiredByURI : Modification de la Data %s/%s"
            % (nklT_prod.BASE_URL, texteIdentifier)
        )
    else:
        logging.error(
            "Bot Nakala isRequiredByURI : Erreur de Modification de la Data %s/%s"
            % (nklT_prod.BASE_URL, texteIdentifier)
        )

def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    args = parser.parse_args()

    logging.info("Démarrage du Bot Nakala Verif isRequiredBy URI")

    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, args.collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, args.collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala Verif isRequiredBy URI : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, args.collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("")
                    Element.fromNakalaDict(element)
                    manuscritIdentifier = Element.getMeta("identifier").split("/")
                    try:
                        listeisRequiredByURI = Element.getMeta("isRequiredByURI")
                    except:
                        logging.error(
                            "Bot Nakala Verif isRequiredBy URI : Erreur sur le chargement de isRequiredBy sur la Page %s/%s"
                            % (nklT_prodEmpty.BASE_URL, identifierisRequiredBy)
                        )
                        listeisRequiredByURIURI = []
                    for isRequiredByURI in listeisRequiredByURI:
                        if "nakala" in isRequiredByURI:
                            ajoutMetadonnnee(manuscritIdentifier, isRequiredByURI)
            else:
                logging.info(
                    "Bot Nakala Verif isRequiredBy URI: Echec du chargement de la collection %s/collection/%s"
                    % (nklT_prodEmpty.BASE_URL, dataIdentifier)
                )
    else:
        logging.error(
            "Bot Nakala Verif isRequiredBy URI: Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, args.collection)
        )


if __name__ == "__main__":
    main()
