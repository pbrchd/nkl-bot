import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import json
import logging
import constants
import argparse
from dateutil.parser import *


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    args = parser.parse_args()

    logging.info("Démarrage du Bot Nakala Verif Date")

    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, args.collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, args.collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala Verif Description : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, args.collection)
                )
                for element in r_current.dictVals["data"]:
                    for ele in element["metas"]:
                        if ele["propertyUri"] == "http://purl.org/dc/terms/date":
                            if "/" in ele["value"]:
                                listeAnnee = ele["value"].split("/")
                                for Annee in listeAnnee:
                                    try:
                                        isoparse(Annee)
                                    except:
                                        logging.error(
                                            "Bot Nakala Verif Date : Element sans Date Valable sur la Page %s/%s"
                                            % (
                                                nklT_prodEmpty.BASE_URL,
                                                element["identifier"],
                                            )
                                        )
                            else:
                                try:
                                    isoparse(ele["value"])
                                except:
                                    logging.error(
                                        "Bot Nakala Verif Date : Element sans Date Valable sur la Page %s/%s"
                                        % (
                                            nklT_prodEmpty.BASE_URL,
                                            element["identifier"],
                                        )
                                    )
            else:
                logging.error(
                    "Bot Nakala Verif Description : Echec du chargement de la collection %s/collection/%s"
                    % (nklT_prodEmpty.BASE_URL, args.collection)
                )
    else:
        logging.error(
            "Bot Nakala Verif Description URI: Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, args.collection)
        )


if __name__ == "__main__":
    main()
