import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import constants
import argparse


def ajoutMetadonnnee(nklT_prodEmpty, dataIdentifier, subject, lang):
    NewElement = NakalaElement(lang)
    NewElement.setMeta("subject", subject)
    dictValsNewMetas = NewElement.getMetaNakala()[0]
    nklD = nklAPI_Datas()
    r_metadata = nklD.post_datas_metadatas(
        nklT_prodEmpty, dataIdentifier, dictValsNewMetas
    )
    if r_metadata.isSuccess:
        logging.info(
            "Bot Nakala Subject : Modification de la Data %s/%s avec %s"
            % (nklT_prodEmpty.BASE_URL, dataIdentifier, subject)
        )
    else:
        logging.error(
            "Bot Nakala Subject : Erreur de Modification de la Data %s/%s avec %s"
            % (nklT_prodEmpty.BASE_URL, dataIdentifier, subject)
        )


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    args = parser.parse_args()

    logging.info("Démarrage du Bot Ajout MotsClefs LaMOP")

    Semantic = SemanticElement("fr")
    LaMOPURI = "http://www.wikidata.org/entity/Q30103052"
    LaMOP = Semantic.interrogation(LaMOPURI)

    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(
        nklT_prodEmpty, args.collection, page=1, limit=20
    )
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, args.collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Nakala Subject : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, args.collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement("fr")
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    listeMotsClefsAjoutes = []
                    listeMotsClefsPresents = Element.getMeta("subject")
                    if "LaMOP" not in listeMotsClefsPresents:
                        ajoutMetadonnnee(nklT_prodEmpty, dataIdentifier, "LaMOP", "fr")
                    if LaMOP not in listeMotsClefsPresents:
                        ajoutMetadonnnee(nklT_prodEmpty, dataIdentifier, LaMOP, "fr")
            else:
                logging.info(
                    "Bot Nakala Subject : Echec du chargement de la collection %s/%s"
                    % (nklT_prodEmpty.BASE_URL, dataIdentifier)
                )
    else:
        logging.error(
            "Bot Nakala Subject : Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, args.collection)
        )


if __name__ == "__main__":
    main()
