import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import json
import logging
import constants
from pygrammalecte import grammalecte_text

def main(args=None):

    BASE_URL = "https://www.nakala.fr"
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verif Description")

    listeMots = []
    f = open('data.json')
    data = json.load(f)
    fichier = open("liste.txt","w")
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            description = Element.getMeta("description")
        except:
            logging.error(
                "Bot Nakala Verif Description : Erreur Pas de description sur la Page %s/%s"
                % (BASE_URL, dataIdentifier)
                )
        else:
            for message in grammalecte_text(description):
                try:
                    #listeMots.append(getattr(message,'word'))
                    fichier.write(getattr(message,'word')+ "\n")
                except:
                    pass
    fichier.close()
if __name__ == "__main__":
    main()
