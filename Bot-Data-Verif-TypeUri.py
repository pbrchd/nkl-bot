import sys

sys.path.append("nakala-pycon")

import json
import logging


def main(args=None):

    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Verif TypeUri")

    f = open('data.json')
    data = json.load(f)
    listetypeUri = []
    for element in data["data"]:
         for ele in element["metas"]:
            try:
                listetypeUri.append(ele["typeUri"])
            except:
                pass
            
    if len(set(listetypeUri)) != 3:
        logging.error(
            "Bot Data Verif KO TypeURI"
            )

if __name__ == "__main__":
    main()
