import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import csv
import json
import logging

def main(args=None):
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    logging.info("Démarrage du Bot Data Get Files")
    f = open('Data/ListeFichier.csv', 'w')
    writer = csv.writer(f)
    header = ["identifiant","Fichier"]
    writer.writerow(header)
    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = "https://www.nakala.fr/" + Element.getMeta("identifier")
        for fichier in element["files"]:
            writer.writerow([dataIdentifier, fichier["name"]])
    f.close()


if __name__ == "__main__":
    main()
