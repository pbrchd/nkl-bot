import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
import csv
import json
import logging

def main(args=None):
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )

    logging.info("Démarrage du Bot Data Get RelationURI")
    BASE_URL = "https://www.nakala.fr"
    f = open('Data/ListeRelationURI.csv', 'w')
    writer = csv.writer(f)
    header = ["identifiant","RelationURI"]
    writer.writerow(header)
    f = open('data.json')
    data = json.load(f)
    for element in data["data"]:
        Element = NakalaElement("fr")
        Element.fromNakalaDict(element)
        dataIdentifier = Element.getMeta("identifier")
        try:
            temp = Element.getMeta("relationURI")
        except:
            logging.error("Probleme : pas de RelationURI %s/%s" % (BASE_URL, Element.getMeta("identifier")))
        finally:
            for element in Element.getMeta("relationURI"):
                    writer.writerow([Element.getMeta("identifier"), element])
    f.close()


if __name__ == "__main__":
    main()
