import sys

sys.path.append("nakala-pycon")

from NakalaElement import NakalaElement
from NakalaCollection import NakalaCollection
from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from nklAPI_Datas import nklAPI_Datas
from SemanticElement import SemanticElement
import time
import json
import logging
import argparse
import constants


def retourneAllNakalaCreators(collection, lang="fr"):
    """retourne un set de tous les creators Nakala present dans les elements de la Collection"""
    listeCollectionNakalaCreators = []
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, collection, page=1, limit=20)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        for i in range(1, lastPage + 1):
            r_current = nklC.get_collections_datas(
                nklT_prodEmpty, collection, page=i, limit=20
            )
            if r_current.isSuccess:
                logging.info(
                    "Bot Creator 2 Collection : Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, collection)
                )
                for element in r_current.dictVals["data"]:
                    Element = NakalaElement(lang)
                    Element.fromNakalaDict(element)
                    dataIdentifier = Element.getMeta("identifier")
                    NakalaCreators = Element.getMeta("nakalacreator")
                    listeCollectionNakalaCreators.extend(NakalaCreators)
            else:
                logging.error(
                    "Bot Creator 2 Collection : Error de Chargement de la Page %s de la Collection %s/collection/%s"
                    % (i, nklT_prodEmpty.BASE_URL, identifier)
                )
    else:
        logging.error(
            "Bot Creator 2 Collection : Error de Chargement de la Collection %s/collection/%s"
            % (nklT_prodEmpty.BASE_URL, collection)
        )
    return set(listeCollectionNakalaCreators)


def retourneListCollectionCreators(collection, lang="fr"):
    """retourne tous les creators Nakala affiche dans les metadonnes de l'element Collection"""
    Element = NakalaCollection(collection, lang=lang)
    return Element.creator


def ajoutCollectionCreators(collection, ListeCreators):
    """ajout des creators dans la collection"""
    Element = NakalaCollection()
    Element.setMeta("creator", ListeCreators)
    nklT_prod = NklTarget(isNakalaProd=True, apiKey=constants.APIKEY_PROD)
    nklC = nklAPI_Collections()
    for element in Element.getMetaNakala():
        r_metadata = nklC.post_collections_metadatas(nklT_prod, collection, element)
        if r_metadata.isSuccess:
            logging.info(
                "Bot Creator 2 Collection : Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, element)
            )
        else:
            logging.error(
                "Bot Creator 2 Collection : Erreur de Modification de la Data %s/collection/%s avec %s"
                % (nklT_prod.BASE_URL, collection, element)
            )


def main(args=None):
    """configuration des logs"""
    logging.basicConfig(
        level=logging.ERROR,
        format="%(asctime)s :: %(levelname)s :: %(message)s",
        filename="debug.log",
    )
    parser = argparse.ArgumentParser()  # Add an argument
    parser.add_argument("--collection", type=str, required=True)  # Parse the argument
    lang = "fr"
    args = parser.parse_args()

    logging.info("Démarrage du Bot Creator 2 Collection")
    listeElementCreators = []
    listeElementNakalaCreators = retourneAllNakalaCreators(
        collection=args.collection, lang=lang
    )

    for Creators in listeElementNakalaCreators:
        if Creators[0] != "" and Creators[1] == "":
            CollectionCreator = Creators[0]
            listeElementCreators.append(CollectionCreator)
        if Creators[1] != "" and Creators[2] == "":
            CollectionCreator = Creators[0] + " " + Creators[1]
            listeElementCreators.append(CollectionCreator)
        if Creators[2] != "":
            CollectionCreator = (
                Creators[0] + " " + Creators[1] + " (" + Creators[2] + ")"
            )
            listeElementCreators.append(CollectionCreator)
    listeCollectionCreators = retourneListCollectionCreators(
        collection=args.collection, lang=lang
    )

    diffCreators = [x for x in listeElementCreators if x not in listeCollectionCreators]
    if len(diffCreators) != 0:
        ajoutCollectionCreators(args.collection, diffCreators)


if __name__ == "__main__":
    main()
